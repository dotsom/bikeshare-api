 #PHILLY BIKESHARE API DOCS

API is listening at 107.170.65.106:9998

##CURRENT ACTIVE API ENDPOINTS -

  Endpoints which require user authentication will be denoted with an `(auth)`.

###USERS-

  All USER API requests require an api key to be sent in the header under the `apikey` key.  The `POST /api/user/auth` endpoint returns a token which should be included in the request header under the `authorization` key.  A new user request will return an initial token, so there is no need to authenticate immediately after creating a user.

####GET-

  +  /api/user/all - **Gets information for all Users**
  +  /api/user/:id - **Where :id is a unique user ID.  Gets information for one User.**
  +  /api/user/email/:email - **Where :email is a url encoded email address.  Gets information for one user**
  +  /api/user/new - **information about required POST params**
  +  /api/user/auth - **information about required POST params**

####POST-

  +  /api/user/new - **Create a new User**
   - required fields:
    {
      email: string,
      password: string,
      firstName: string,
      lastName: string,
      username: string
    }

  +  /api/user/auth - **Authenticate an existing User**
   - required fields
    {
      email: string,
      password: string
    }

  +  /api/user/deauth - **Deauthenticate a logged in User**
   - requires authenticated token be included in the `authorization` header

  +  /api/user/password/token - **Create and save a new password token**
   - required fields
    {
      email: string
    }

####PUT-

  +  /api/user/password/reset - **Change a user's password**
    - requires password token be included in the `passwordtoken` header
    - required fields
      {
        email: string,
        password : string
      }

####DELETE-

  +  /api/user/:id - **Where :id is a unique user ID.  Delete an existing User**

  ________

BIKES-

  GET  /api/bike/all
  GET  /api/bike/:id where :id is a unique bike ID
  GET  /api/bike/new (information about POST params)
  POST /api/bike/new
    required fields {
      count: Int: How many new bikes you want to create
    }

CONSTANTS-

  GET  /api/bike/all
  GET  /api/bike/:key where :key is an identifying string

RACKS-

  GET  /api/rack/all
  GET  /api/rack/:id where :id is a unique rack ID
  GET  /api/rack/new (information about POST params)
  POST /api/rack/new
    required fields {
      lat: string,
      lng: string,
      name: string,
      maxBikes: int,
      availBikes: int
    }

RIDES-

  GET  /api/ride/all
  GET  /api/ride/:id where :id is a unique ride ID
  GET  /api/ride/user/:uid where :uid is a unique user ID
  GET  /api/ride/new (information about POST params)
  POST /api/ride/new
    required fields {
      userId: "String: Unique ID of user",
      bikeId: "OPTIONAL- String: Unique ID of bike",
      startRackId: "String: Unique ID of starting rack",
      endRackId: "String: Unique ID of ending rack",
      seconds: "Int: Duration of trip in seconds",
      distance: "Int: Distance of trip in miles"
    }
