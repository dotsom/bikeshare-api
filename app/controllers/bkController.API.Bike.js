module.exports = function (
    router,
    Bike,
    Constant
) {

    //GET request for all bikes
    router.get('/bike/all', function (req, res) {
        Bike.find({}, function (err, bikes) {
            if (err) {
                res.status(200).send({
                    err: true,
                    message: 'Error Encountered',
                    type: 'Bike',
                    data: err
                });
            } else {
                res.status(200).send({
                    err: false,
                    message: 'All Bikes',
                    type: 'Bike',
                    data: bikes
                });
            }
        });
    });

    //GET request for POST information
    router.get('/bike/new', function (req, res) {
      res.status(200).send({
          err: false,
          message: 'The following fields are expected for a successful POST request to this URL',
          type: 'Bike',
          data: {
            count: "Int: Number of bikes to create",
          }
      });
    });

    //GET request for one bike
    router.get('/bike/:id', function (req, res) {
        Bike.findById(req.params.id, function (err, bike) {
            if (err) {
                console.log(err);
            } else {
                res.status(200).send({
                    err: false,
                    message: 'Bike ' + req.params.id,
                    type: 'Bike',
                    data: bike
                });
            }
        });
    });

    //POST request for new Bike
    router.post('/bike/new', function (req, res) {
        Constant.findOne({key: 'firstNames'}, function(err, names){
            var bikeArray = [];

            var count = Number(req.body.count);

            for (var i = 0; i < count; i++){
                var rand = Math.floor(Math.random() * 100);

                console.log(names);

                bikeArray.push({
                    name: names.value[rand],
                    dob: new Date()
                });
            }

            Bike.collection.insert(bikeArray, function(err, bikes){
                if (err) {
                  res.status(200).send({
                    err: true,
                    message: 'Save failed',
                    type: 'Bike',
                    data: err
                  });
                } else {
                  res.status(200).send({
                    err: false,
                    message: 'Bikes saved',
                    type: 'Bike',
                    data: bikes
                  });
                }
            });
        });
    });
};