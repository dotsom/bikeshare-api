module.exports = function (
    router,
    Constant
) {

    //GET request for all constants
    router.get('/constant/all', function (req, res) {
        Constant.find({}, function (err, constants) {
            if (err) {
                console.log(err);
            } else {
                res.status(200).send({
                    err: false,
                    message: 'All Constants',
                    type: 'Constant',
                    data: constants
                });
            }
        });
    });

    //GET request for one constant
    router.get('/constant/:key', function (req, res) {
        Constant.findOne({key: req.params.key}, function (err, constant) {
            if (err) {
                console.log(err);
            } else {
                res.status(200).send({
                    err: false,
                    message: 'Constants["' + req.params.key + '"]' ,
                    type: 'Constant',
                    data: constant
                });
            }
        });
    });
};