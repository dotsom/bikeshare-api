module.exports = function (
    router,
    auth,
    util,
    mailer,
    Rack
) {

    var resType = 'Rack';

    //GET request for all racks
    router.get('/rack/all',  auth.client, function (req, res) {
        Rack.find({}, function (err, racks) {
            if (err) {
                util.handleError(res, err, resType);
            } else {
                util.handleSuccess(res, 'All Racks', resType, racks);
            }
        });
    });

    //GET request for one rack
    router.get('/rack/:id', auth.client, function (req, res) {
        Rack.findById(req.params.id, function (err, rack) {
            if (err) {
                util.handleError(res, err, resType);
            } else {
                util.handleSuccess(res, 'Rack ' + req.params.id, resType, rack);
            }
        });
    });

    /*
    router.post('/rack/new', auth.client, function (req, res) {
        var newRack = new Rack(),
            errorCount = 0,
            errorMsgs = [];

        if (req.body.lat) {
            newRack.lat = req.body.lat;
        } else {
            errorCount++;
            errorMsgs.push('You need to include a value "lat"');
        }

        if (req.body.lng) {
            newRack.lng = req.body.lng;
        } else {
            errorCount++;
            errorMsgs.push('You need to include a value "lng"');
        }

        if (req.body.name) {
            newRack.name = req.body.name;
        } else {
            errorCount++;
            errorMsgs.push('You need to include a value "name"');
        }

        if (req.body.maxBikes) {
            newRack.maxBikes = req.body.maxBikes;
        } else {
            errorCount++;
            errorMsgs.push('You need to include a value "maxBikes"');
        }

        if (req.body.availBikes) {
            newRack.availBikes = req.body.availBikes;
        } else {
            errorCount++;
            errorMsgs.push('You need to include a value "availBikes"');
        }

        newRack.availTime = new Date;

        if (errorCount < 1) {
            newRack.save(function (err) {
                if (err) {
                  res.status(200).send({
                    err: true,
                    message: 'Save failed',
                    type: 'Rack',
                    data: err
                  });
                } else {
                  res.status(200).send({
                    err: false,
                    message: 'User saved',
                    type: 'Rack',
                    data: newRack
                  });
                }
            });
        } else {
            res.status(200).send({
                err: true,
                message: 'Error Encountered',
                type: 'Rack',
                data: errorMsgs
            });
        }
    });
*/
};