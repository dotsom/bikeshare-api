module.exports = function (
    router,
    moment,
    Ride,
    User
) {

    //GET request for all rides
    router.get('/ride/all', function (req, res) {
        Ride.find({}, function (err, rides) {
            if (err) {
                console.log(err);
            } else {
                var ridesArray = [];

                for (var i = 0; i < rides.length; i++ ) {
                    var thisRide = {
                        startTime : moment(rides[i].startTime).format('YYYY-MM-DD HH:mm:ss Z'),
                        endTime : moment(rides[i].endTime).format('YYYY-MM-DD HH:mm:ss Z'),
                        bikeId : typeof rides[i].bikeId == 'undefined' ? false : rides[i].bikeId,
                        userId : rides[i].userId,
                        startRackId : rides[i].startRackId,
                        endRackId : rides[i].endRackId,
                        _id : rides[i]._id,
                        distance : rides[i].distance
                    };

                    ridesArray.push(thisRide);
                }

                res.status(200).send({
                    err: false,
                    message: 'All Rides',
                    type: 'Ride',
                    data: ridesArray
                });
            }
        });
    });

    //GET request for rides for a user
    router.get('/ride/new', function (req, res) {
      res.status(200).send({
          err: false,
          message: 'The following fields are expected for a successful POST request to this URL',
          type: 'Ride',
          data: {
            userId: "String: Unique ID of user",
            bikeId: "OPTIONAL- String: Unique ID of bike",
            startRackId: "String: Unique ID of starting rack",
            endRackId: "String: Unique ID of ending rack",
            seconds: "Int: Duration of trip in seconds",
            startTime: "String: Date in the format we discussed",
            distance: "Int: Distance of trip in miles"
          }
      });
    });

    //GET request for one ride
    router.get('/ride/:id', function (req, res) {
        Ride.findById(req.params.id, function (err, ride) {
            if (err) {
                console.log(err);
            } else {

                var thisRide = {
                    startTime : moment(ride.startTime).format('YYYY-MM-DD HH:mm:ss Z'),
                    endTime : moment(ride.endTime).format('YYYY-MM-DD HH:mm:ss Z'),
                    bikeId : typeof ride.bikeId == 'undefined' ? false : rides[i].bikeId,
                    userId : ride.userId,
                    startRackId : ride.startRackId,
                    endRackId : ride.endRackId,
                    _id : ride._id,
                    distance : ride.distance
                };

                res.status(200).send({
                    err: false,
                    message: 'Ride ' + req.params.id,
                    type: 'Ride',
                    data: thisRide
                });
            }
        });
    });

    //GET request for rides for a user
    router.get('/ride/user/:uid', function (req, res) {
        Ride.find({userId: req.params.uid}, function (err, rides) {
            if (err) {
                console.log(err);
            } else {

                var ridesArray = [];

                for (var i = 0; i < rides.length; i++ ) {
                    var thisRide = {
                        startTime : moment(rides[i].startTime).format('YYYY-MM-DD HH:mm:ss Z'),
                        endTime : moment(rides[i].endTime).format('YYYY-MM-DD HH:mm:ss Z'),
                        bikeId : typeof rides[i].bikeId == 'undefined' ? false : rides[i].bikeId,
                        userId : rides[i].userId,
                        startRackId : rides[i].startRackId,
                        endRackId : rides[i].endRackId,
                        _id : rides[i]._id,
                        distance : rides[i].distance
                    };

                    ridesArray.push(thisRide);
                }

                res.status(200).send({
                    err: false,
                    message: 'Rides for User ' + req.params.uid,
                    type: 'Ride',
                    data: ridesArray
                });
            }
        });
    });

    //POST request for new Ride
    router.post('/ride/new', function (req, res) {
        var newRide = new Ride(),
            errorCount = 0,
            errorMsgs = [];

        if (req.body.userId) {
            User.findById(req.body.userId, function(err, user){

                if (err) {
                    res.status(200).send({
                        err: true,
                        message: 'User Query Error',
                        type: 'User',
                        data: err
                    });

                } else if (!user) {
                    res.status(200).send({
                        err: true,
                        message: 'User Not Found',
                        type: 'User',
                        data: 'A user with the unique id ' + req.body.userId + ' could not be found'
                    });

                } else {

                    newRide.userId = req.body.userId;

                    if (req.body.bikeId) {
                        newRide.bikeId = req.body.bikeId;
                    }

                    if (req.body.startRackId) {
                        newRide.startRackId = req.body.startRackId;
                    } else {
                        errorCount++;
                        errorMsgs.push('You need to include a value "startRackId"');
                    }

                    if (req.body.endRackId) {
                        newRide.endRackId = req.body.endRackId;
                    } else {
                        errorCount++;
                        errorMsgs.push('You need to include a value "startRackId"');
                    }

                    if (req.body.startTime && req.body.seconds) {
                        var startTime = moment(req.body.startTime, 'YYYY-MM-DD HH:mm:ss Z');
                        var endTime = moment(startTime).add({seconds: Number(req.body.seconds)});
                        newRide.startTime = startTime.toDate();
                        newRide.endTime = endTime.toDate();
                    } else {
                        errorCount++;
                        errorMsgs.push('You need to include a value "startTime" and a value "seconds"');
                    }

                    if (typeof req.body.distance != "undefined") {
                        newRide.distance = req.body.distance;
                    } else {
                        errorCount++;
                        errorMsgs.push('You need to include a value "distance"');
                    }

                    newRide.availTime = new Date;

                    if (errorCount < 1) {
                        newRide.save(function (err) {
                            if (err) {
                              res.status(200).send({
                                err: true,
                                message: 'Ride Save Failed',
                                type: 'Ride',
                                data: err
                              });
                            } else {
                                user.rides++;
                                user.distance.total += Number(newRide.distance);
                                user.distance.average = user.distance.total/user.rides;
                                user.lastAction = new Date();

                                user.save(function(err){
                                    if (err) {
                                        res.status(200).send({
                                            err: true,
                                            message: 'User Save Failed',
                                            type: 'User',
                                            data: err
                                        });
                                    } else {
                                        res.status(200).send({
                                            err: false,
                                            message: 'Ride saved',
                                            type: 'Ride',
                                            data: {
                                                ride: newRide,
                                                user: user
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    } else {
                        res.status(200).send({
                            err: true,
                            message: 'Error encountered',
                            type: 'Ride',
                            data: errorMsgs
                        });
                    }
                }
            });
        } else {
            errorCount++;
            errorMsgs.push('You need to include a value "userId"');

            res.status(200).send({
                err: true,
                message: 'Error encountered',
                type: 'Ride',
                data: errorMsgs
            });
        }
    });
};