module.exports = function (
    router,
    auth,
    util,
    mailer,
    User
) {

  var scrubSensitiveData = {
    password: 0,
    token: 0,
    pwToken: 0
  },
  resType = 'User';

  //GET request for POST instructions
  router.get('/user/new', auth.client, function (req, res) {
    var msg = 'The following fields are expected for a successful POST request to this URL',
      data = {
        email: "String: Email address",
        password: "String: User password",
        firstName: "String: User first name",
        lastName: "String: User last name"
      };

    util.handleSuccess(res, msg, resType, data);
  });

  //GET request for POST instructions
  router.get('/user/auth', auth.client, function (req, res) {
    var msg = 'The following fields are expected for a successful POST request to this URL',
      data = {
          email: "String: Email address",
          password: "String: User password"
        };

    util.handleSuccess(res, msg, resType, data);
  });

  //GET request for all
  router.get('/user/all', auth.client, function (req, res) {
    User.find({}, scrubSensitiveData, function (err, allUsers) {
        if (err) {
          util.handleError(res, err, resType);
        } else {
          util.handleSuccess(res, 'All Users', resType, allUsers);
        }
    });
  });

  //GET request for one user by id
  router.get('/user/:id', auth.client, function (req, res) {
    User.findById(req.params.id, scrubSensitiveData,function (err, user) {
      if (err) {
        util.handleError(res, err, resType);
      } else {
        util.handleSuccess(res, 'User ' + req.params.id, resType, user);
      }
    });
  });

  //GET request for one user by email
  router.get('/user/email/:email', auth.client, function (req, res) {
    var emailString = decodeURI(req.params.email);
    User.findOne({email: emailString}, scrubSensitiveData, function (err, user) {
      if (err) {
        util.handleError(res, err, resType);
      } else {
        util.handleSuccess(res, 'User ' + req.params.email, resType, user);
      }
    });
  });

  //POST request to authenticate User
  router.post('/user/auth', auth.client, function (req, res) {
    User.findOne({$or: [{email: req.body.email }, {username: req.body.email}]}, function(err, user) {
      // error
      if (err) {
        util.handleError(res, err, resType);
      } else if (!user) {
        util.handleError(res, 'That username and password combination could not be authenticated', resType);
      } else if (!user.validPassword(req.body.password)) {
        util.handleError(res, 'That username and password combination could not be authenticated', resType);
      } else {
        var unhashed = user.generateNewToken();
        user.save(function(err){
          if (err) {
            util.handleError(res, err, resType);
          } else {
            user.password = undefined;
            user.token = user._id + unhashed;
            util.handleSuccess(res, 'User authenticated', resType, user);
          }
        });
      }
    });
  });

  //POST request to deauthenticate User
  router.post('/user/deauth', auth.client, auth.user, function (req, res) {
    User.findById(req.user._id, function(err, user) {
      if (err) {
        util.handleError(res, err, resType);
      } else if (!user) {
        util.handleError(res, 'Could not find user to deauthenticate.', resType);
      } else {
        user.deauthenticate(req, res);
      }
    });
  });

  //POST request to create and send new password reset token
  router.post('/user/password/token', auth.client, function (req, res) {
    User.findOne({$or: [{email: req.body.email }, {username: req.body.email}]}, function(err, user) {
      // error
      if (err) {
        util.handleError(res, err, resType);
      } else if (!user) {
        util.handleError(res, 'That email address does not have an associated account.', resType);
      } else {
        var unhashed = user.generateNewPasswordToken(14);
        user.save(function(err){
          if (err) {
            util.handleError(res, err, resType);
          } else {
            var newMail = mailer(),
                message_html = "<h2>Reset Your Password</h2>"
                + "<p>A request to change your password has been initiated.  Please copy the following 6 digit code and paste it into the corresponding field inside the app to continue.  If you did not request a new password, please ignore this email.</p>"
                + "<ul><li>Code: " + unhashed + "</li></ul>",
                to_name = user.firstName + " " + user.lastName,
                from_name = 'Indego2go',
                from_email = "message.noreply@indego2go.com";

            newMail.setContent(message_html, 'Password Reset', user.email, from_email, to_name, from_name);
            newMail.send(function(err){
              if (err) {
                util.handleError(res, err, resType);
              } else {
                util.handleSuccess(res, 'Password reset token sent', resType);
              }
            });
          }
        });
      }
    });
  });

  //PUT request to accept a pwToken and change password
  router.put('/user/password/token', auth.client, auth.pwToken, function (req, res) {
    if (req.body.password) {
      req.user.savePassword(req.body.password);
      req.user.save(function(err){
        if (err) {
          util.handleError(res, err, resType);
        } else {
          util.handleSuccess(res, 'New password saved', resType);
        }
      });
    } else {
      util.handleError(res, 'Request must contain a new password in the body under `password`',resType);
    }
  });

  //PUT request to save new password while logged in
  router.put('/user/password/reset', auth.client, auth.user, function (req, res) {
    if (req.body.oldpassword && req.user.validPassword(req.body.oldpassword)) {
      if (req.body.newpassword) {
        req.user.savePassword(req.body.newpassword);
        req.user.save(function(err){
          if (err) {
            util.handleError(res, err, resType);
          } else {
            util.handleSuccess(res, 'New password saved', resType);
          }
        });
      } else {
        util.handleError(res, 'Request must contain a new password in the body under `newpassword`', resType);
      }
    } else {
      util.handleError(res, 'Request must contain the existing password in the body under `oldpassword`', resType);
    }
  });

  //POST request for new User
  router.post('/user/new', auth.client, function (req, res) {
    User.findOne({$or: [{email: req.body.email }, {username: req.body.username}]}, function(err, user) {
      if (err) {
        util.handleError(res, err, resType);
      } if (user) {
        util.handleError(res, 'That email or username already has an associated account', resType);
      } else {
        var newUser = new User(),
          errorMsgs = [],
          unhashed;

        if (req.body.firstName) {
          newUser.firstName = req.body.firstName;
        } else {
          errorMsgs.push('You need to include a value "firstName"');
        }

        if (req.body.lastName) {
          newUser.lastName = req.body.lastName;
        } else {
          errorMsgs.push('You need to include a value "lastName"');
        }

        if (req.body.username) {
          newUser.username = req.body.username;
        } else {
          errorMsgs.push('You need to include a value "username"');
        }

        if (req.body.email) {
          newUser.email = req.body.email;
        } else {
          errorMsgs.push('You need to include a value "email"');
        }

        if (req.body.password) {
          newUser.savePassword(req.body.password);
        } else {
          errorMsgs.push('You need to include a value "email"');
        }

        unhashed = newUser.generateNewToken();
        newUser.distance.total = 0;
        newUser.distance.average = 0;
        newUser.rides = 0;

        if (errorMsgs.length < 1) {
          newUser.save(function (err) {
            if (err) {
              util.handleError(res, err, resType);
            } else {
              newUser.password = undefined;
              newUser.token = newUser._id + unhashed;
              util.handleSuccess(res, 'User creation successful', resType, newUser);
            }
          });
        } else {
          util.handleError(res, errorMsgs, resType);
        }
      }
    });
  });

  //DELETE request to delete an active User
  router.delete('/user/:id', auth.client, function (req, res) {
    User.findById(req.params.id, scrubSensitiveData, function(err, user) {
      // error
      if (err) {
        util.handleError(res, err, resType);
      } else if (!user) {
        util.handleError(res, 'Could not find a User with that ID', resType);
      } else {
        user.remove(function(err){
          if (err) {
            util.handleError(res, err, resType);
          } else {
            util.handleSuccess(res, 'User removal successful', resType, user._id);
          }
        });
      }
    });
  });
};