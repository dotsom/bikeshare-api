module.exports = function (
    router,
    moment,
    _,
    http,
    auth,
    util,
    mailer,
// Schema
    Bike,
    Constant,
    Rack,
    RequestedRack,
    Ride,
    User
) {
    'use strict';

    router.get('/', function (req, res) {
        res.json({ message: 'Please consult the docs to use this API!' });
    });

    // Bike Controller
    require('./controllers/bkController.API.Bike')(
        router,
        Bike,
        Constant
    );

    // Constant Controller
    require('./controllers/bkController.API.Constant')(
        router,
        Constant
    );

    // Rack Controller
    require('./controllers/bkController.API.Rack')(
        router,
        auth,
        util,
        mailer,
        Rack
    );

    // Ride Controller
    require('./controllers/bkController.API.Ride')(
        router,
        moment,
        Ride,
        User
    );

    // User Controller
    require('./controllers/bkController.API.User')(
        router,
        auth,
        util,
        mailer,
        User
    );
};


