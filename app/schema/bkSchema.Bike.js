var mongoose = require('mongoose');

var bikeSchema = mongoose.Schema({
  dob : { type: Date, required: true, default: new Date() },
  name : { type: String, required: true, trim: true },
});

module.exports = mongoose.model('Bike', bikeSchema);