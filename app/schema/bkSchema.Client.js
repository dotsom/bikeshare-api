var mongoose = require('mongoose'),
    bcrypt   = require('bcrypt-nodejs'),
    crypto   = require('crypto');

var clientSchema = mongoose.Schema({
  name           : { type: String, required: true, trim: true },
  key            : { type: String, required: true, trim: true },
  permission     : { type: Number, required: true},
});

clientSchema.methods.verifyKey = function(key) {
  return bcrypt.compareSync(key, this.key);
};

clientSchema.methods.generateNewKey = function() {
  var unhashed = crypto.randomBytes(20).toString('hex');
  this.key = bcrypt.hashSync(unhashed, bcrypt.genSaltSync(8), null);
  return unhashed;
};

// create the model for users and expose it to our app
module.exports = mongoose.model('Client', clientSchema);