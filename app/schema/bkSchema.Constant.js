var mongoose = require('mongoose');

var constantSchema = mongoose.Schema({
  key : { type: String, required: true, trim: true },
  value : mongoose.Schema.Types.Mixed
});

// create the model for constants and expose it to our app
module.exports = mongoose.model('Constant', constantSchema);