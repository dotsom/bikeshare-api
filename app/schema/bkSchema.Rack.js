var mongoose = require('mongoose');

var rackSchema = mongoose.Schema({
  name : { type: String, required: true, trim: true },
  status : { type: String, required: true, trim: true },
  timezone : { type: String, required: true, trim: true },
  capacity : { type: Number, required: true, trim: true },
  k_id : { type: String, required: true, trim: true },
  geometry : {
    coords : { type: [], required: true },
  },
  available : {
    bikes : { type: Number, required: true, trim: true },
    slots : { type: Number, required: true, trim: true }
  },
  address : {
    street : { type: String, required: false },
    city : { type: String, required: false },
    state : { type: String, required: false },
    zip : { type: String, required: false }
  }
});

// checking if password is valid
rackSchema.methods.updateRack = function(rack) {
  this.name = rack.properties.name;
  this.status = rack.properties.kioskPublicStatus;
  this.timezone = rack.properties.timeZone;
  this.capacity = rack.properties.totalDocks;
  this.k_id = rack.properties.kioskId;
  this.geometry = { coords: rack.geometry.coordinates };
  this.available = {
    bikes: rack.properties.bikesAvailable,
    slots: rack.properties.docksAvailable
  };
  this.address = {
    street: rack.properties.addressStreet,
    city: rack.properties.addressCity,
    state: rack.properties.addressState,
    zip: rack.properties.addressZipCode
  };

  return this;
};

module.exports = mongoose.model('Rack', rackSchema);

/**********************************
***
***  This is an expected rack object
***  from a typical JSON request.
***

{
      "geometry": {
        "coordinates": [
          -75.16374,
          39.95378
        ],
        "type": "Point"
      },
      "properties": {
        "addressStreet": "1401 John F. Kennedy Blvd.",
        "addressCity": "Philadelphia",
        "addressState": "PA",
        "addressZipCode": "19102",
        "bikesAvailable": 1,
        "closeTime": "23:58:00",
        "docksAvailable": 22,
        "eventEnd": null,
        "eventStart": null,
        "isEventBased": false,
        "isVirtual": false,
        "kioskId": 3004,
        "kioskPublicStatus": "Active",
        "name": "Municipal Services Building Plaza",
        "openTime": "00:02:00",
        "publicText": "",
        "timeZone": "Eastern Standard Time",
        "totalDocks": 23,
        "trikesAvailable": 0
      },
      "type": "Feature"
    }*/