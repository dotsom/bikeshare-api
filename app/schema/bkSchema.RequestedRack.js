var mongoose = require('mongoose');

var requestedRackSchema = mongoose.Schema({
  lng : { type: String, required: true, trim: true },
  lat : { type: String, required: true, trim: true },
  approved : { type: Boolean, required: true, default: false },
  userId : { type: String, required: true, trim: true },
  votes : []
});

// create the model for requestedRacks and expose it to our app
module.exports = mongoose.model('RequestedRack', requestedRackSchema);