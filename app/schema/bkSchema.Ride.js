var mongoose = require('mongoose');

var rideSchema = mongoose.Schema({
  userId             : { type: String, required: true, trim: true },
  bikeId             : { type: String, required: false, trim: true },
  startRackId        : { type: String, required: true, trim: true },
  endRackId          : { type: String, required: true, trim: true },
  startTime          : { type: Date, required: true },
  endTime            : { type: Date, required: true },
  distance           : { type: Number, required: true },
  calories           : { type: Number, required: false }
});

// create the model for rides and expose it to our app
module.exports = mongoose.model('Ride', rideSchema);