var mongoose = require('mongoose');

var sampleSchema = mongoose.Schema({
    name : { type: String, required: true, trim: true }
});

module.exports = mongoose.model('Sample', sampleSchema);
