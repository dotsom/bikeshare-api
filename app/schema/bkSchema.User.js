var mongoose = require('mongoose'),
    bcrypt   = require('bcrypt-nodejs'),
    crypto   = require('crypto'),
    moment   = require('moment'),
    util     = require('../../custom_modules/utility.js');

var userSchema = mongoose.Schema({
  email           : { type: String, required: true, trim: true },
  username        : { type: String, required: true, trim: true },
  password        : { type: String, required: true, trim: true },
  firstName       : { type: String, required: true, trim: true },
  lastName        : { type: String, required: true, trim: true },
  lastAction      : { type: Date, required: true, default: new Date },
  lastLogout      : { type: Date, required: false },
  friends         : [],
  distance        : {
    average : { type: Number, required: true, default: 0 },
    total : { type: Number, required: true, default: 0 }
  },
  token : {},
  rides : { type: Number, required: true, default: 0 },
  pwToken : {
    active : { type: Boolean, required: true, default: false },
    token : { type: String, required: false, trim: true },
    created : { type: Date, required: false},
    expires : { type: Date, required: false},
  }
});

// generating a hash
userSchema.methods.savePassword = function(password) {
  this.password = bcrypt.hashSync(password, bcrypt.genSaltSync(8), null)
  return this.password;
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.password);
};

// checking if password is valid
userSchema.methods.verifyToken = function(token) {
  return this.token === token;
};

// checking if password is valid
userSchema.methods.generateNewToken = function() {
  var unhashed = crypto.randomBytes(20).toString('hex');
  this.token = bcrypt.hashSync(unhashed, bcrypt.genSaltSync(8), null)
  return unhashed;
};

// deauthenticate a user
userSchema.methods.deauthenticate = function(req, res, next) {
  var resType = 'User';
  this.token = false;
  this.lastLogout = new Date;

  this.save(function(err){
    if (err) {
      util.handleError(res, err, resType);
    } else {
      util.handleSuccess(res, 'User deauthenticated', resType, false, function(){
        if(next) {
          next();
        }
      });
    }
  });
};

userSchema.methods.tokenMatchesSaved = function(token) {
  return bcrypt.compareSync(token, this.pwToken.token);
};

userSchema.methods.tokenIsExpired = function(token) {
    var today = moment(),
    expires = moment(this.pwToken.expires);

  if (today.isBefore(expires)) {
    return false;
  } else {
    return true;
  }
};

userSchema.methods.generateNewPasswordToken = function(daysToExpiry) {
  var unhashed = crypto.randomBytes(3).toString('hex'),
  today = new Date(),
  expiryDate = moment(today).add(daysToExpiry, 'days').toDate();

  this.pwToken.token = bcrypt.hashSync(unhashed, bcrypt.genSaltSync(8), null);
  this.pwToken.expires = expiryDate;
  this.pwToken.created = today;
  this.pwToken.active = true;

  return unhashed;
};

userSchema.methods.retirePasswordToken = function(token) {
  this.pwToken.token = undefined;
  this.pwToken.expires = undefined;
  this.pwToken.created = undefined;
  this.pwToken.active = false;
};

module.exports = mongoose.model('User', userSchema);