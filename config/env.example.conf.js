'use strict';

module.exports = {
  env: 'Your Environment',
  port: 80,
  os: 'Your OS',
  ssl: false
};