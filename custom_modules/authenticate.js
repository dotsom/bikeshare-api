var util = require('./utility.js'),
    bcrypt   = require('bcrypt-nodejs');

module.exports = function(User, Client){

  function rehashTokens(unhashed) {
    var hashed = bcrypt.hashSync(unhashed, bcrypt.genSaltSync(8), null);
    return hashed;
  }

  return {
    user: function (req, res, next) {
      req.user = {};
      if (req.headers.authorization) {
        var _id = req.headers.authorization.substring(0, 24),
            token = req.headers.authorization.substring(24, 64);

        User.findById(_id, function(err, user) {
          if (user) {
            if (user.token == false) {
              util.handleError(res, 'This user is in a logged-out state and must authenticate before accessing this endpoint.', 'User');
            } else if (bcrypt.compareSync(token, user.token)){
              req.user = user;
              req.user.authenticated = true;
              next();
            } else {
              util.handleError(res, 'That token did not match the stored token', 'User');
            }
          } else if (err) {
            util.handleError(res, err, 'User');
          } else {
            util.handleError(res, 'That is not a valid token format', 'User');
          }
        });
      } else {
        util.handleError(res, 'A valid authenticated token must be included in the request\'s `authorization` header.', 'User');
      }
    },

    client: function (req, res, next) {
      if (req.headers.apikey) {
        var _id = req.headers.apikey.substring(0, 24),
            key = req.headers.apikey.substring(24, 64);

        Client.findById(_id, function(err, client) {
          if (client) {
            if (bcrypt.compareSync(key, client.key)){
              req.client = client;
              req.client.authenticated = true;
              next();
            } else {
              util.handleError(res, 'Could not authenticate that api key', 'Client');
            }
          } else if (err) {
            util.handleError(res, err, 'Client');
          } else {
            util.handleError(res, 'That is not a valid api-key', 'Client');
          }
        });
      } else {
        util.handleError(res, 'A valid api-key must be included in the request\'s `apikey` header', 'Client');
      }
    },

    pwToken: function (req, res, next) {
      if (req.headers.passwordtoken && req.body.email) {
        User.findOne({$or: [{email: req.body.email }, {username: req.body.email}]}, function(err, user) {
          if (user) {
            if (user.pwToken.active) {
              if (user.tokenMatchesSaved(req.headers.passwordtoken) && !user.tokenIsExpired()){
                user.retirePasswordToken();
                req.user = user;
                next();
              } else if (user.tokenIsExpired()) {
                util.handleError(res, 'That token has expired', 'Password Reset');
              } else {
                util.handleError(res, 'That token does not match the token on record', 'Password Reset');
              }
            } else {
              util.handleError(res, 'That user doesn\'t have any open password reset requests', 'Password Reset');
            }
          } else if (err) {
            util.handleError(res, err, 'Password Reset');
          } else {
            util.handleError(res, 'That is not a known user email', 'Password Reset');
          }
        });
      } else if (!req.headers.passwordtoken) {
        util.handleError(res, 'A valid 6 character password reset token must be included in the request\'s `passwordtoken` header', 'Password Reset');
      } else {
        util.handleError(res, 'A valid email address must be included in the request\'s body under `email`', 'Password Reset');
      }
    },
  }
};

