var util = require('./utility.js'),
    resType = 'Mailer Module';

var mailer = function (){
    var apikey = '7BzcCNailMlLHtGQ8xkheQ';
    var newMail = {

        mandrill : require('node-mandrill')(apikey),

        setContent: function(message, subject, toEmail, fromEmail, toName, fromName){
            this.email = {
                key: apikey,
                message: {
                    ready: true,
                    html: message,
                    subject: subject,
                    from_email: fromEmail,
                    from_name: fromName,
                    to: [
                        {
                            email: toEmail,
                            name: toName,
                            type: "to"
                        }
                    ],
                    headers: {
                        "Reply-To": "message.noreply@indego2go.com"
                    }
                },
                async: false,
                ip_pool: "Main Pool"
            }

            return this;
        },

        send : function(next){
            this.mandrill('/messages/send', this.email, function(err, m_res){
                if(next) {
                    next(err, m_res);
                }
            });
        }
    };

    return newMail;
};

module.exports = mailer;