module.exports = function (dir, root, targets, cb) {
    var i = 0,
        streams = [],
        fs = require('fs'),
        request = require('request'),
        mkdirp = require('mkdirp');

		function ensureExists(path, cb) {
		    mkdirp(path, function(err) {
		        if (err) {
		            if (err.code === 'EEXIST') {
		            	cb(null);
		            } else {
		            	cb(err);
		            }
		        } else {
		        	cb(null);
		        }
		    });
		}

    	function executionWrapper(i, targets, segments){

			ensureExists(dir + '/' + targets[i], function(err) {
			    if (err) {
			    	console.log(err);
			    } else {
    				var stream = fs.createWriteStream(dir + '/' + targets[i] + '/index.html');
			        stream.once('open', function (fd) {
			            request(root + '/' + targets[i], function (err, res, body) {
			                if (err) {
			                    console.log(err);
			                    return false;
			                } else if (res.statusCode === 200) {
			                    stream.end(body);
			                    if (cb && i == targets.length - 1) {
			                        cb();
			                    }
			                } else {
			                	console.log('Server responded with code ' + res.statusCode + '...')
			                }
			            });
			        });
			    }
			});
    	}

    while (i < targets.length) {
    	var segments = targets[i].split('/');
    	executionWrapper(i, targets, segments);
    	i++;
    }
};