

module.exports = function (target, interval, cb) {
  var i = 0,
      util = require('./utility.js'),
      mailer = require('./mailer.js'),
      request = require('request'),
      newMail = mailer(),
      to_name = "Thomas Peduto",
      to_email = "thomas.peduto@gmail.com",
      from_name = 'Bikeshare API Watchdog',
      from_email = "watchdog@indego2go.com";

  request(target, function (err, res, body) {
      if (err) {

        var message_html = "<h2>Error in the Questioner Module @ Bikeshare API</h2><p>The server has failed to sync data with the official API on the rack level. Server responded with an error:  " + err + "</p>";
        console.log('Questioner error: ' + err);

        newMail.setContent(
          message_html,
          'Error in the Questioner Module',
          to_email,
          from_email,
          to_name,
          from_name
        ).send(function(err){
          if (!err) {
            clearInterval(interval);
            console.log('Error report sent.');
          }
        });
      } else if (res.statusCode === 200) {

          var update = JSON.parse(body);
          var err = null;
          if (cb) {
              cb(err, update);
          }
      } else {

        var message_html = "<h2>Error in the Questioner Module @ Bikeshare API</h2>"
            + "<p>The server has failed to sync data with the official API on the rack level.  Server responded with code " + res.statusCode + ".</p>";
        console.log('Questioner error: Server responded with code ' + res.statusCode + '...');

        newMail.setContent(
          message_html,
          'Error in the Questioner Module',
          to_email,
          from_email,
          to_name,
          from_name
        ).send(function(err){
          if (!err) {
            clearInterval(interval);
            console.log('Error report sent.');
          }
        });
      }
  });

};