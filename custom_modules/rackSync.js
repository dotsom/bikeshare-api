var url = 'https://www.rideindego.com/stations/json/';
var syncEvery = 10 * 1000;
var Rack = require('../app/schema/bkSchema.Rack');
var questioner = require('./questioner.js');
var _ = require('underscore');
module.exports = function () {
    console.log('Booting sync script...');
    var syncRacks = setInterval(function(){
        questioner(url, syncRacks, function(err, update){
            if (!err) {
                _.each(update.features, function(update){
                    Rack.findOne({ k_id: update.properties.kioskId }, function(err, rack){
                        if (err) {
                            console.log(err);
                        } else if (!rack) {
                            var newRack = new Rack();
                            newRack.updateRack(update);
                            newRack.save(function(err){
                                if (err) {
                                    console.log(err);
                                }
                            });
                        } else {
                            rack.updateRack(update);
                            rack.save(function(err){
                                if (err) {
                                    console.log(err);
                                }
                            });
                        }
                    });
                });
            }
        });
    }, syncEvery);
};