module.exports = {
  handleError: function(res, err, type, cb) {
    if (err) {
      res.status(200).send({
        err: true,
        message: 'Error encountered',
        type: type,
        data: err
      });
    } else if (cb) {
      cb();
    }
  },

  handleSuccess: function(res, msg, type, data, cb) {
    res.status(200).send({
        err: false,
        message: msg,
        type: type,
        data: data ? data : undefined
    });

    if (cb) {
      cb();
    }
  },
};