// Packages
    bcrypt = require('bcrypt-nodejs'),
    mongoose = require('mongoose'),
    configDB = require('./config/database.conf.js'),
    prompt = require('prompt'),

// Schema
    Client = require('./app/schema/bkSchema.Client');

// Connect to MongoDB via Mongoose
mongoose.connect(configDB.url);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback() {
    prompt.start();
    prompt.get(['name'], function (err, result) {
        if (err) {
            console.log(err);
        } else {
            var newClient = new Client(),
              errorCount = 0,
              errorMsgs = [];

            if (result.name) {
              newClient.name = result.name;
            } else {
              errorCount++;
              errorMsgs.push('You need to enter a valid Client name.');
            }

            newClient.permission = 1;
            var unhashed = newClient.generateNewKey();

            if (errorCount) {
                console.log(errorMsgs[0]);
            } else {
                newClient.save(function(err){
                    if (err) console.log(err);
                    else {
                        console.log("Your API Key: " + newClient._id + unhashed);
                        process.exit();
                    }
                });
            }
        }
    });
});

