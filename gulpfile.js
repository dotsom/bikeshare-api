var gulp = require('gulp');
var gulpSSH = require('gulp-ssh')({
  ignoreErrors: false,
  sshConfig: {
    host: '104.131.225.143',
    port: 22,
    username: 'root',
    password: 'housecatbuckwheatpoisonberry'
  }
});

// execute commands
gulp.task('deploy', function () {
  return gulpSSH
    .shell([
      'cd /var/www/bikeshare-api',
      'git pull origin master',
      'npm install',
      'su tpeduto',
      'forever stopall',
      'forever start server.js'
    ], {filePath: 'commands.log', autoExit: true})
    .pipe(gulp.dest('logs'));
});