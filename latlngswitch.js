// Port
var port = process.env.PORT || 9999,

// Packages
    express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    apiRouter = express.Router(),
    mongoose = require('mongoose'),
    configDB = require('./config/database.js'),
    _ = require('underscore'),

// Schema
    Rack = require('./app/schema/bkSchema.Rack'),
    Ride = require('./app/schema/bkSchema.Ride'),
    User = require('./app/schema/bkSchema.User');

// Connect to MongoDB via Mongoose
mongoose.connect(configDB.url);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback() {
    console.log('Successful connection to mongodb at ' + configDB.url + '...');
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

Rack.find({}, function(err, racks){
    _.each(racks, function(rack){
        var switcharoo = {
            lat: rack.lng,
            lng: rack.lat
        }

        rack.lng = switcharoo.lng;
        rack.lat = switcharoo.lat;

        rack.save(function(err){
            if(err){
                console.log(err)
            } else {
                console.log(rack);
            }
        });
    });
});

// Launch
app.listen(port);
console.log('Listening on port ' + port + '...');