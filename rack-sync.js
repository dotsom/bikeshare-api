// Port
var express = require('express'),
    app = express(),
    bcrypt = require('bcrypt-nodejs'),
    bodyParser = require('body-parser'),
    apiRouter = express.Router(),
    mongoose = require('mongoose'),
    configDB = require('./config/database.conf.js'),
    moment = require('moment'),
    _ = require('underscore'),
    http = require('http'),

// Schema
    Bike = require('./app/schema/bkSchema.Bike'),
    Client = require('./app/schema/bkSchema.Client'),
    Constant = require('./app/schema/bkSchema.Constant'),
    Rack = require('./app/schema/bkSchema.Rack'),
    RequestedRack = require('./app/schema/bkSchema.RequestedRack'),
    Ride = require('./app/schema/bkSchema.Ride'),
    User = require('./app/schema/bkSchema.User');

// Connect to MongoDB via Mongoose
mongoose.connect(configDB.url);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback() {
    console.log('Successful connection to mongodb at ' + configDB.url + '...');
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
});

// Route level middleware
var auth = require('./custom_modules/authenticate.js')(User, Client),
util = require('./custom_modules/utility.js'),
mailer = require('./custom_modules/mailer.js');

// Routes
require('./app/router.API.js')(
    apiRouter,
    moment,
    _,
    http,
    auth,
    util,
    mailer,
    Bike,
    Constant,
    Rack,
    RequestedRack,
    Ride,
    User
);

app.use('/API', apiRouter);

// Launch
app.listen(port);
console.log('Listening on port ' + port + '...');