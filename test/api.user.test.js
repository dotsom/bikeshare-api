'use strict';

var superagent = require('superagent'),
    expect = require('expect'),
    config = require('../config/test.conf.js'),
    thisUser = {};

describe('Bikeshare User API Endpoints', function(){
  it('POST - /api/user/new - Create new User', function(done){

    superagent.post(config.servername + 'api/user/new')

    .set('apikey', config.apiKey)

    .send({
      firstName: config.user.firstName,
      lastName: config.user.lastName,
      email: config.user.email,
      password: config.user.password
    })

    .end(function(e,res){
      expect(res).toExist;
      //Status
      expect(res.status).toEqual(200);
      //Errors
      expect(res.body.err).toEqual(false);
      //Message
      expect(res.body.message).toEqual('User creation successful');
      //Type
      expect(res.body.type).toEqual('User');
      //Data
        expect(res.body.data._id.length).toEqual(24);
        expect(res.body.data.token.length).toEqual(64);
        expect(res.body.data.firstName).toEqual('Thomas');
        expect(res.body.data.lastName).toEqual('Bombadil');
        expect(res.body.data.email).toEqual('tom.bombadil@valar.edu');
        expect(res.body.data.password).toNotExist();
        expect(res.body.data.rides).toEqual(0);
        expect(res.body.data.friends).toEqual([]);
        expect(res.body.data.lastAction).toBeA('string');
          expect(res.body.data.distance.average).toEqual(0);
          expect(res.body.data.distance.total).toEqual(0);
      //Set for later
      thisUser._id = res.body.data._id;
      thisUser.token = res.body.data.token;
      thisUser.user = res.body.data;
      done();
    });
  });

  it('GET - /api/user/all - Get all Users', function(done){

    superagent.get(config.servername + 'api/user/all')

    .set('apikey', config.apiKey)

    .end(function(e,res){
      expect(res).toExist;
      //Status
      expect(res.status).toEqual(200);
      //Errors
      expect(res.body.err).toEqual(false);
      //Message
      expect(res.body.message).toEqual('All Users');
      //Type
      expect(res.body.type).toEqual('User');
      //Data
      expect(typeof res.body.data).toEqual('object');
      done();
    });
  });

  it('GET - /api/user/:id - Get one User by id', function(done){

    superagent.get(config.servername + 'api/user/' + thisUser._id)

    .set('apikey', config.apiKey)

    .end(function(e,res){
      expect(res).toExist;
      //Status
      expect(res.status).toEqual(200);
      //Errors
      expect(res.body.err).toEqual(false);
      //Message
      expect(res.body.message).toEqual('User ' + thisUser._id);
      //Type
      expect(res.body.type).toEqual('User');
      //Data
        expect(res.body.data._id.length).toEqual(24);
        expect(res.body.data.firstName).toEqual('Thomas');
        expect(res.body.data.lastName).toEqual('Bombadil');
        expect(res.body.data.email).toEqual('tom.bombadil@valar.edu');
        expect(res.body.data.password).toNotExist();
        expect(res.body.data.rides).toEqual(0);
        expect(res.body.data.friends).toEqual([]);
        expect(res.body.data.lastAction).toBeA('string');
          expect(res.body.data.distance.average).toEqual(0);
          expect(res.body.data.distance.total).toEqual(0);
      done();
    });
  });

  it('GET - /api/user/email/:email - Get one User by email', function(done){

    superagent.get(config.servername + 'api/user/email/' + encodeURI(thisUser.user.email))

    .set('apikey', config.apiKey)

    .end(function(e,res){
      expect(res).toExist;
      //Status
      expect(res.status).toEqual(200);
      //Errors
      expect(res.body.err).toEqual(false);
      //Message
      expect(res.body.message).toEqual('User ' + thisUser.user.email);
      //Type
      expect(res.body.type).toEqual('User');
      //Data
        expect(res.body.data._id.length).toEqual(24);
        expect(res.body.data.firstName).toEqual('Thomas');
        expect(res.body.data.lastName).toEqual('Bombadil');
        expect(res.body.data.email).toEqual('tom.bombadil@valar.edu');
        expect(res.body.data.password).toNotExist();
        expect(res.body.data.rides).toEqual(0);
        expect(res.body.data.friends).toEqual([]);
        expect(res.body.data.lastAction).toBeA('string');
          expect(res.body.data.distance.average).toEqual(0);
          expect(res.body.data.distance.total).toEqual(0);
      done();
    });
  });

  it('POST - /api/user/auth - Authenticate user, return new token', function(done){

    superagent.post(config.servername + 'api/user/auth')

    .set('apikey', config.apiKey)
    .send({
      email: config.user.email,
      password: config.user.password
    })

    .end(function(e,res){
      expect(res).toExist;
      //Status
      expect(res.status).toEqual(200);
      //Errors
      expect(res.body.err).toEqual(false);
      //Message
      expect(res.body.message).toEqual('User authenticated');
      //Type
      expect(res.body.type).toEqual('User');
      //Data
        expect(res.body.data._id.length).toEqual(24);
        expect(res.body.data.firstName).toEqual('Thomas');
        expect(res.body.data.lastName).toEqual('Bombadil');
        expect(res.body.data.email).toEqual('tom.bombadil@valar.edu');
        expect(res.body.data.password).toNotExist();
        expect(res.body.data.token).toNotEqual(thisUser.token);
        expect(res.body.data.rides).toEqual(0);
        expect(res.body.data.friends).toEqual([]);
        expect(res.body.data.lastAction).toBeA('string');
          expect(res.body.data.distance.average).toEqual(0);
          expect(res.body.data.distance.total).toEqual(0);

      thisUser.token = res.body.data.token;
      done();
    });
  });

  it('POST - /api/user/deauth - Deauthenticate user', function(done){

    superagent.post(config.servername + 'api/user/deauth')

    .set('apikey', config.apiKey)
    .set('authorization', thisUser.token)
    .end(function(e,res){
      expect(res).toExist;
      //Status
      expect(res.status).toEqual(200);
      //Errors
      expect(res.body.err).toEqual(false);
      //Message
      expect(res.body.message).toEqual('User deauthenticated');
      //Type
      expect(res.body.type).toEqual('User');
      done();
    });
  });

  it('GET - /api/user/new - Instructions for POST to create User', function(done){

    superagent.get(config.servername + 'api/user/new')

    .set('apikey', config.apiKey)

    .end(function(e,res){
      expect(res).toExist;
      //Status
      expect(res.status).toEqual(200);
      //Errors
      expect(res.body.err).toEqual(false);
      //Message
      expect(res.body.message).toEqual('The following fields are expected for a successful POST request to this URL');
      //Type
      expect(res.body.type).toEqual('User');
      //Data
      expect(typeof res.body.data).toEqual('object');
      expect(res.body.data.email).toEqual('String: Email address');
      expect(res.body.data.password).toEqual('String: User password');
      expect(res.body.data.firstName).toEqual('String: User first name');
      expect(res.body.data.lastName).toEqual('String: User last name');
      done();
    });
  });

  it('GET - /api/user/auth - Instructions for POST to authenticate User', function(done){

    superagent.get(config.servername + 'api/user/auth')

    .set('apikey', config.apiKey)

    .end(function(e,res){
      expect(res).toExist;
      //Status
      expect(res.status).toEqual(200);
      //Errors
      expect(res.body.err).toEqual(false);
      //Message
      expect(res.body.message).toEqual('The following fields are expected for a successful POST request to this URL');
      //Type
      expect(res.body.type).toEqual('User');
      //Data
      expect(typeof res.body.data).toEqual('object');
      expect(res.body.data.email).toEqual('String: Email address');
      expect(res.body.data.password).toEqual('String: User password');
      done();
    });
  });

  it('DELETE - /api/user/:_id - Delete user', function(done){

    superagent.del(config.servername + 'api/user/' + thisUser._id)

    .set('apikey', config.apiKey)

    .end(function(e,res){
      expect(res).toExist;
      //Status
      expect(res.status).toEqual(200);
      //Errors
      expect(res.body.err).toEqual(false);
      //Message
      expect(res.body.message).toEqual('User removal successful');
      //Type
      expect(res.body.type).toEqual('User');
      //Data
      expect(typeof res.body.data).toEqual('string');
      expect(res.body.data).toEqual(thisUser._id);
      done();
    });
  });
});